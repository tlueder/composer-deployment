FROM alpine:3.14
LABEL Maintainer="Thomas Lüder <t.lueder@nets.de>" \
    Description="Lightweight container based on Alpine Linux."

# Install Depencies
RUN apk update && apk upgrade && \
    apk add --no-cache \
    bash git openssh openssh-client rsync lftp nodejs npm gzip \
    php8 php8-cli php8-bcmath php8-bz2 php8-common php8-curl php8-ctype  \
    php8-dom php8-fileinfo php8-gd php8-imap php8-intl php8-json php8-mbstring php8-opcache  \
    php8-pdo php8-pspell php8-simplexml php8-snmp php8-soap php8-sqlite3 php8-tidy  \
    php8-tokenizer php8-xml php8-xmlreader php8-xmlwriter php8-zip php8-phar php8-openssl php8-session php8-iconv \
    curl 

# Install Sass Compiler
RUN npm install -g sass

# Install SVGO
RUN npm install -g svgo

RUN printf '#!/bin/sh'"\n\nsvgo \$1 -o \$2\n" > /bin/svgcleaner && chmod u+x /bin/svgcleaner

# Remove old php
RUN if [ -f /usr/bin/php ]; then rm /usr/bin/php; fi

# Link
RUN ln -s /usr/bin/php8 /usr/bin/php

RUN php --version

# Install composer from the official image
COPY --from=composer:latest /usr/bin/composer /usr/bin/composer
